#include <iostream>
#include <cstdlib>
#include <chrono>
#include <thread>

const int SIZE = 3;
const char EMPTY_CELL = '*';
bool showColAndRowNumbers = false;

void displayWelcomeScreen();
void cleanDisplay();
void displayBoard(const char board[SIZE][SIZE]);
bool isBadCellCoordinates(int row, int col);
bool isAlreadyOccupiedCell(const char board[SIZE][SIZE], int row, int col);
bool isCurrentPlayerWin(const char board[SIZE][SIZE], char player);

int main()
{
  char board[SIZE][SIZE] = {{EMPTY_CELL, EMPTY_CELL, EMPTY_CELL}, {EMPTY_CELL, EMPTY_CELL, EMPTY_CELL}, {EMPTY_CELL, EMPTY_CELL, EMPTY_CELL}};
  char currentPlayer = 'X';
  int moveCount = 0;
  int col, row;

  displayWelcomeScreen();

  while (true)
  {
    cleanDisplay();
    displayBoard(board);
    std::cout << std::endl;
    std::cout << "The palayer: " << currentPlayer << ". Enter Row and Col numbers: ";
    std::cin >> row >> col;
    if (isBadCellCoordinates(row, col))
    {
      std::cout << "You entered wrong cell, row and col should be numbers from 1 to 3. Please try again" << std::endl;
      continue;
    }
    else if (isAlreadyOccupiedCell(board, row, col))
    {
      std::cout << "You selected already occupied cell, Please select another one" << std::endl;
      continue;
    }

    board[row - 1][col - 1] = currentPlayer;
    moveCount++;
    if (isCurrentPlayerWin(board, currentPlayer))
    {
      std::cout << "Player: " << currentPlayer << " won!" << std::endl;
      break;
    }
    else if (moveCount == SIZE * SIZE)
    {
      std::cout << "Draw!" << std::endl;
      break;
    }

    currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
  }

  return 0;
}

bool isCurrentPlayerWin(const char board[SIZE][SIZE], char player)
{
  for (int i = 0; i < SIZE; i++)
  {
    if (board[i][0] == player && board[i][1] == player && board[i][2] == player)
      return true;
    if (board[0][i] == player && board[1][i] == player && board[2][i] == player)
      return true;
  }
  if (board[0][0] == player && board[1][1] == player && board[2][2] == player)
    return true;
  if (board[0][2] == player && board[1][1] == player && board[2][0] == player)
    return true;
  return false;
}

bool isAlreadyOccupiedCell(const char board[SIZE][SIZE], int row, int col)
{
  return board[row - 1][col - 1] != EMPTY_CELL;
}

bool isBadCellCoordinates(int row, int col)
{
  return row < 1 || row > SIZE || col < 1 || col > SIZE;
}

void displayWelcomeScreen()
{
  std::cout << "Tic-Tac-Toe Game\n";
  std::cout << "Rules of the Game:\n";
  std::cout << "1. The game is intended for two players.\n";
  std::cout << "2. The first player uses 'X', the second uses 'O'.\n";
  std::cout << "3. Players take turns entering coordinates to place their symbol on the field.\n";
  std::cout << "4. The player who first forms an unbroken line of three of their symbols wins.\n";
  std::cout << "Produced by @zloyleva.\n";
  std::cout << "Press Enter to start the game...\n";
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

void cleanDisplay()
{
  std::this_thread::sleep_for(std::chrono::seconds(1));
  system("clear"); // it's just for UNIX
}

void displayBoard(const char board[SIZE][SIZE])
{
  if (showColAndRowNumbers)
  {
    std::cout << "    1 2 3" << std::endl;
    std::cout << "    - - -" << std::endl;
  }

  for (int i = 0; i < SIZE; i++)
  {
    if (showColAndRowNumbers)
    {
      std::cout << i + 1 << " | ";
    }
    for (int j = 0; j < SIZE; j++)
    {
      std::cout << board[i][j] << " ";
    }
    std::cout << std::endl;
  }
}